package sample;

public class Abstraction {

    abstract class A{

        abstract void display();
        void show(){

            System.out.println("SHOW METHOD");
        }
    }

    static class B extends Abstraction{

        void display(){

            System.out.println("Abstract Method in A");
        }
        void show(){

            System.out.println("SHOW METHOD");
        }
    }



static class AbstractMain{

        public static void main(String[] args){

            B obj = new B();
            obj.display();
            obj.show();
        }

    }

}
