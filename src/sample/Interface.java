package sample;

public interface Interface {

    interface A{
        void Adisplay();
        void Ashow();
    }

    interface B{
        void Bdisplay();
        void Bshow();
    }

    interface AB extends A,B{

        default void Adisplay(){

            System.out.println("A DISPLAY");

        }

        default void Ashow(){

            System.out.println("A SHOW");
        }

        default void Bdisplay(){

            System.out.println("B DISPLAY");

        }

        default void Bshow(){

            System.out.println("B SHOW");
        }

    }

    class  interface InterfaceA,Interface B{

        public static void main(String[] args){

            AB obj = new AB();
            obj.Adisplay();
            obj.Ashow();
            obj.Bdisplay();
            obj.Bshow();

        }
    }
}
